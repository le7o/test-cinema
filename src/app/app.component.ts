import {Component, OnDestroy, OnInit} from '@angular/core';

import {Observable, Subject} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';

import {DataService} from './shared/services/data.service';
import {InitService} from './shared/services/init.service';

import {IFilm} from './shared/models/film';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  private readonly onDestroy$ = new Subject<void>();

  selectedDate: Date;
  dateList: Array<{ date: Date }> = [];
  filmList$: Observable<IFilm[]>;

  constructor(private initService: InitService,
              private dataService: DataService) {
  }

  ngOnInit(): void {
    this.dataService.getSchedule()
      .pipe(
        map((dateList) => {
          this.selectDate(dateList[0].date);
          return dateList;
        }),
        takeUntil(this.onDestroy$)
      )
      .subscribe(list => {
        this.dateList = list;
      });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  selectDate(item: Date): void {
    if (this.selectedDate !== item) {
      this.selectedDate = item;
      this.filmList$ = this.dataService.getFilm(this.selectedDate);
    }
  }

  selectPlace(event: any): void {
    const {sessionDate, sessionTime, seat} = event;
    this.dataService.setPlace(sessionDate, sessionTime, seat);
  }
}
