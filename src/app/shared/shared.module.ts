import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MovieSessionComponent } from './components/movie-session/movie-session.component';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
  declarations: [
    MovieSessionComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MovieSessionComponent,
    HeaderComponent
  ]
})
export class SharedModule {
}
