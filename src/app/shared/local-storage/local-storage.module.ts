import {NgModule, Optional, SkipSelf} from '@angular/core';
import {LocalStorageService, PREFIX} from './local-storage.service';

@NgModule()
export class LocalStorageModule {

  constructor(@Optional() @SkipSelf() parentModule: LocalStorageModule) {
    if (parentModule) {
      throw new Error('LocalStorageModule is already loaded. Import only in Root App Module');
    }
  }

  static forRoot(prefix?: string) {
    return {
      ngModule: LocalStorageModule,
      providers: [
        {
          provide: PREFIX,
          useValue: !prefix ? 'STORE_' : prefix
        },
        LocalStorageService
      ]
    };
  }
}
