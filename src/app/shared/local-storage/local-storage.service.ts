import {Inject, Injectable, InjectionToken} from '@angular/core';

export const PREFIX = new InjectionToken<string>('PREFIX');

@Injectable()
export class LocalStorageService {

  private readonly prefix;

  constructor(@Inject(PREFIX) private storagePrefix: string) {
    this.prefix = storagePrefix;
  }

  public set(key: String, value: any): void {
    return localStorage.setItem(this.prefix + key, JSON.stringify(value));
  }

  public get(key: String): any | null {
    return JSON.parse(localStorage.getItem(this.prefix + key));
  }

  public delete(key: String): void {
    localStorage.removeItem(this.prefix + key);
  }
}
