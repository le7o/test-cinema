import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IFilm, IFilmSession} from '../../models/film';

import * as moment from 'moment';

@Component({
  selector: 'app-movie-session',
  templateUrl: './movie-session.component.html',
  styleUrls: ['./movie-session.component.scss']
})
export class MovieSessionComponent {

  @Input()
  film: IFilm;

  @Output()
  selectPlaceEvent = new EventEmitter();

  selectedTime: IFilmSession;

  get isArchive(): boolean {
    return moment().isAfter(moment(this.film.date + ' ' + this.selectedTime.sessionTime));
  }

  constructor() {
  }

  selectSessionTime(time: any) {
    this.selectedTime = time;
  }

  selectPlace(seat: any): void {
    if (seat.isFree && !this.isArchive) {

      this.selectPlaceEvent.emit({
        sessionDate: this.film.date,
        sessionTime: this.selectedTime.sessionTime,
        seat
      });

      const timeIdx = this.film.sessions.findIndex(s => s.sessionTime === this.selectedTime.sessionTime);
      this.film.sessions[timeIdx].schedule[seat.row - 1][seat.place - 1].isFree = false;
    }
  }
}
