import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  @Input()
  list: Array<{ date: Date }>;

  @Input()
  selectedDate: Date;

  @Output()
  selectDateEvent = new EventEmitter();

  selectDate(date: Date): void {
    this.selectDateEvent.emit(date);
  }
}
