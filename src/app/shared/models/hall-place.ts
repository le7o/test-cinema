export interface IHallPlace {
  row: number;
  place: number;
  isFree: boolean;
}
