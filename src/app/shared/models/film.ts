import {IHallPlace} from './hall-place';


export interface IFilmSession {
  sessionTime: Date;
  schedule: IHallPlace[];
}

export interface IFilm {
  date: Date;
  name: string;
  description: string;
  sessions: IFilmSession[];
}

