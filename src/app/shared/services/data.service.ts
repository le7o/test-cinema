import {Injectable} from '@angular/core';

import {BehaviorSubject, Observable} from 'rxjs';

import {LocalStorageService} from '../local-storage/local-storage.service';

import {IFilm} from '../models/film';
import {IHallPlace} from '../models/hall-place';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private readonly storePrefix = 'CINEMA';

  constructor(private localStorageService: LocalStorageService) {
  }

  getSchedule(): Observable<Array<{ date: Date }>> {
    const response = new BehaviorSubject(null);
    const dateList = this.localStorageService.get(this.storePrefix);

    response.next(dateList.map((item) => ({date: item.date})));

    return response.asObservable();
  }

  getFilm(sessionDate: Date): Observable<IFilm[]> {
    const response = new BehaviorSubject(null);
    const schedule = this.localStorageService.get(this.storePrefix);

    const filmList = schedule
      .filter(item => item.date === sessionDate)
      .map(film => ({
        date: film.date,
        name: film.name,
        description: film.description,
        sessions: film.sessions
      }));

    response.next(filmList);
    return response.asObservable();
  }

  setPlace(sessionDate: Date, sessionTime: String, seat: IHallPlace) {
    const schedule = this.localStorageService.get(this.storePrefix);

    const filmIdx = schedule.findIndex(film => film.date === sessionDate);
    const candidateFilm = schedule[filmIdx];

    const sessionIdx = candidateFilm.sessions.findIndex(s => s.sessionTime === sessionTime);
    const session = candidateFilm.sessions[sessionIdx];

    session.schedule[seat.row - 1][seat.place - 1].isFree = false;
    candidateFilm.sessions[sessionIdx] = session;

    schedule[filmIdx] = candidateFilm;

    this.localStorageService.set(this.storePrefix, schedule);
  }
}
