import {Injectable} from '@angular/core';

import {LocalStorageService} from '../local-storage/local-storage.service';

import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class InitService {

  constructor(private localStorageService: LocalStorageService) {
    this.init();
  }

  private init() {
    const dates: Array<String> = [];
    const sessions: Array<String> = [];

    Array.from({length: 7}, (i, j) => dates.push(moment().add(j, 'days').format('YYYY-MM-DD')));

    let currentTime = moment('10:00', 'HH:mm');
    Array.from({length: 6}, (i, j) => {
      if (j === 0) {
        sessions.push(currentTime.format('HH:mm'));
      } else {
        currentTime = currentTime.add(2, 'hours');
        sessions.push(currentTime.format('HH:mm'));
      }
    });


    const storeData = dates.map((date) => ({
      date,
      name: 'Однажды в Голивуде',
      description: `Фильм повествует о череде событий, произошедших в Голливуде в 1969 году, на закате его «золотого века». По
          сюжету, известный ТВ актер Рик Далтон и его дублер Клифф Бут пытаются найти свое место в стремительно
          меняющемся мире киноиндустрии.`,
      sessions: sessions.map((time) => ({
        sessionTime: time,
        schedule: this.createCinemaSession()
      }))
    }));

    const store = this.localStorageService.get('CINEMA');
    if (!store) {
      this.localStorageService.set('CINEMA', storeData);
    }
  }

  private createCinemaSession() {
    const schedule = [];
    const rows = [6, 10, 14, 14, 14, 14, 14, 16, 18];
    rows.forEach((row, index) => {
      schedule[index] = [];
      for (let i = 0; i < row; i++) {
        schedule[index][i] = {
          row: index + 1,
          place: i + 1,
          isFree: true
        };
      }
    });
    return schedule;
  }
}


